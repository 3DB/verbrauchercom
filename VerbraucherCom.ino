#include <Arduino.h>

// Defines
#define CP_READ_ITERATIONS 100  // How often CP should be read

// Pins
#define CP_READ A2  // Control pilot read (Analog)
#define RELAY 8   // Start charging pin
#define LED_GREEN 3     // LED charging
#define LED_RED 4    // LED Default
#define END_CHARGING 23 // Button to end charging

// Debug output
#define VOLTAGES_CP true
#define INIT true
#define MAIN true
#define PWM true

enum State {STANDBY = 0, VEHICLE_DETECTED = 1, READY = 2, WITH_VENTILATION = 3, NO_POWER = 4, FAILED = 5};
const String FIRMWARE_VERSION = "0.2";  // Current firmware version

bool g_pwm_is_active = false;
bool g_charging_led = false;
bool g_charging_led_is_on = false;
State curState = STANDBY;

void setup() {
    // Set serial baud rate
    Serial.begin(9600);

    // Startup message
    String str = "Starting up VehicleCom v" + FIRMWARE_VERSION + "...";
    println(str, INIT);

    // Init pins
    println("Setting up PINS...", INIT);
    pinMode(CP_READ, INPUT);
    pinMode(RELAY, OUTPUT);
    pinMode(LED_RED, OUTPUT);
    pinMode(LED_GREEN, OUTPUT);
    pinMode(END_CHARGING, INPUT);

    // Start red LED
    digitalWrite(LED_RED, HIGH);

    // Begins in STANDBY
    println("Starts in STANDBY", INIT);
}

void loop() {
    State newState = cpRead();

    // States
    switch (curState) {
        case STANDBY:
            if (newState == VEHICLE_DETECTED) {
                println("Switched to: VEHICLE_DETECTED", MAIN);
                curState = VEHICLE_DETECTED;
            }
            break;
        
        case VEHICLE_DETECTED:
            if (g_pwm_is_active) {
                println("Switched to: READY", MAIN);
                digitalWrite(RELAY, HIGH);
                g_charging_led = true;
                curState = READY;
            }
            break;
        
        case READY:
            if (!g_pwm_is_active || endCharging()) {
                println("Switched to: STANDBY", MAIN);
                g_charging_led = false;
                digitalWrite(RELAY, LOW);
                curState = STANDBY;
            }
            break;
    }

    // Charging LED
    if (g_charging_led) {
        if (millis() % 300) {
            if (g_charging_led_is_on) {
                digitalWrite(LED_GREEN, LOW);
                digitalWrite(LED_RED, HIGH);
            } else {
                digitalWrite(LED_GREEN, HIGH);
                digitalWrite(LED_RED, LOW);
            }

            g_charging_led_is_on = !g_charging_led_is_on;
        }
    } else {
        digitalWrite(LED_GREEN, LOW);
        digitalWrite(LED_RED, HIGH);
    }
}

/**
* Checks whether user wants to end charging
*/
bool endCharging() {
   return analogRead(END_CHARGING) > 800;
}

/**
 * Reads CP's voltage and returns the corresponding state.
 */
enum State cpRead() {
    int voltage;
    int pLow = 1023;
    int pHigh = 0;
    enum State newState = FAILED;

    // 0V/0 = -12V; 1023/5V = 12V
    for (int i = 0; i < CP_READ_ITERATIONS; i++) {
        voltage = analogRead(CP_READ);
        println("Voltage: " + String(voltage) + "V", VOLTAGES_CP);
        
        if (voltage > pHigh)
            pHigh = voltage;
      
        else if (voltage < pLow)
            pLow = voltage;
    }

    if (pHigh < 962 && pHigh > 899)
        newState = STANDBY;

    else if (pHigh < 868 && pHigh > 806)
        newState = VEHICLE_DETECTED;

    else if (pHigh < 775 && pHigh > 712)
        newState = READY;

    else if (pHigh < 681 && pHigh > 619)
        newState = WITH_VENTILATION;

    else if (pHigh < 588 && pHigh > 525)
        newState = NO_POWER;

    if (151 < pLow && pLow < 214) {
        if (!g_pwm_is_active)
            println("PWM signal started!", PWM);
        g_pwm_is_active = true;
    } else {
        if (g_pwm_is_active)
            println("PWM signal turned off!", PWM);
        g_pwm_is_active = false;
    }

    // Return new state
    return newState;
}

void println(String text, bool pd) {
    print(text + "\n", pd);
}

void print(String text, bool pd) {
    if (pd) {
        Serial.print(text);
    }
}
